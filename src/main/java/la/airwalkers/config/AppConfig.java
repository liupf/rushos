package la.airwalkers.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@ComponentScan(basePackages = "la.airwalkers")
@PropertySource("classpath:app.properties")
public class AppConfig {
    @Value("${app.version}")
    private String version;

    @Value("${driver.name}")
    private String driverName;

    @Value("${driver.path}")
    private String driverPath;

    public String getVersion() {
        return version;
    }

    public String getDriverName() {
        return driverName;
    }

    public String getDriverPath() {
        return driverPath;
    }
}
