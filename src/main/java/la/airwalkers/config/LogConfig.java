package la.airwalkers.config;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

@Configuration
public class LogConfig {
    @Bean
    public Logger logger() throws IOException {
        Properties properties = new Properties();
        properties.load(getClass().getResourceAsStream("/log4j.properties"));
        PropertyConfigurator.configure(properties);
        return Logger.getLogger(LogConfig.class);
    }
}
