package la.airwalkers.entity;

public class RecordDTO {
    private Long id;
    private String region;
    private String title;
    private String layout;
    private Integer floor;
    private Float area;
    private Float price;

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public Float getArea() {
        return area;
    }

    public void setArea(Float area) {
        this.area = area;
    }

    @Override
    public String toString() {
        return "RecordDTO{" +
                "id=" + id +
                ", region='" + region + '\'' +
                ", title='" + title + '\'' +
                ", layout='" + layout + '\'' +
                ", floor=" + floor +
                ", area=" + area +
                ", price=" + price +
                '}';
    }
}
