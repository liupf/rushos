package la.airwalkers.rush;

/**
 * @author liupf
 */
public interface Task {
    /**
     * 用于TaskService的接口
     */
    void run();
}
